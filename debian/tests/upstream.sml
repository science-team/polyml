val _ = use "Tests/RunTests";

fun main () =
  let
    val status = if runTests "Tests" then OS.Process.success
                 else OS.Process.failure;
    val _ = OS.Process.exit(status);
  in
    raise Fail "OS.Process.exit did not terminate the process!"
  end
