polyml (5.8.1-1~exp1) experimental; urgency=medium

  * New upstream version 5.8.1
  * Drop patches fixed upstream
  * riscv-libffi.patch: Refresh
  * Rename libpolyml9 to libpolyml11 to match new soname

 -- Jessica Clarke <jrtc27@debian.org>  Sat, 28 Nov 2020 05:02:25 +0000

polyml (5.7.1-4) unstable; urgency=medium

  * libffi-3.3-i386.patch: Fix any-i386 build after upstream libffi-3.3.patch

 -- James Clarke <jrtc27@debian.org>  Sun, 19 Jan 2020 15:11:46 +0000

polyml (5.7.1-3) unstable; urgency=medium

  * libffi-3.3.patch: Backport from upstream to fix build with libffi 3.3
    (Closes: #949290)

 -- James Clarke <jrtc27@debian.org>  Sun, 19 Jan 2020 12:06:58 +0000

polyml (5.7.1-2) unstable; urgency=medium

  * Bump standards version to 4.1.4, no changes needed
  * Use https for copyright format URI
  * d/control:
    - Change Vcs-* to point to salsa
    - Add `Rules-Requires-Root: no`
  * Add RISC-V support

 -- James Clarke <jrtc27@debian.org>  Sat, 30 Jun 2018 17:08:06 +0100

polyml (5.7.1-1) unstable; urgency=medium

  * New upstream version 5.7.1
  * Bump standards version to 4.1.2, no changes needed
  * Delete unused Test166-deadlock.patch from packaging

 -- James Clarke <jrtc27@debian.org>  Fri, 08 Dec 2017 16:43:55 +0000

polyml (5.7.1~20171120.git44b7b88-1) experimental; urgency=medium

  * New upstream version 5.7.1~20171120.git44b7b88
    - Closes: #880023
  * Rename libpolyml8 to libpolyml9 to reflect upstream soname bump
  * Split modules out of libpolyml9 into new polyml-modules
  * Drop all patches; fixed upstream
  * Add new modules-non-executable.patch to make module files non-executable

 -- James Clarke <jrtc27@debian.org>  Mon, 20 Nov 2017 14:50:57 +0000

polyml (5.7-2) unstable; urgency=medium

  * Bump standards version to 4.1.1, no changes needed
  * d/control:
    - Add Breaks/Replaces on libpolyml7
    - Remove Testsuite field; now automatically added by dpkg
    - Drop dh-autoreconf dependency; redundant since dehelper 10
  * d/rules:
    - Drop explicit --with autoreconf; redundant since compat 10

 -- James Clarke <jrtc27@debian.org>  Thu, 05 Oct 2017 15:25:54 +0100

polyml (5.7-1) unstable; urgency=medium

  * Update uscan pattern to match 5.7 filename
  * New upstream version 5.7
  * Test166-deadlock.patch: Backport upstream fix for Test166 deadlocking
  * Bumped standards version to 4.0.1
    - Changed priority to optional

 -- James Clarke <jrtc27@debian.org>  Fri, 18 Aug 2017 23:57:36 +0100

polyml (5.7~20170322.git096d446-1) experimental; urgency=medium

  * New upstream version 5.7~20170322.git096d446
  * debian/rules: Revert recompiling the compiler itself, since git snapshots
    now have up-to-date pre-built import files.
  * Rename libpolyml7 to libpolyml8 to reflect upstream soname bump

 -- James Clarke <jrtc27@debian.org>  Thu, 23 Mar 2017 12:04:38 +0000

polyml (5.7~20170315.git1343f60-1) experimental; urgency=medium

  * New upstream version 5.7~20170315.git1343f60
  * Drop all patches; fixed upstream
  * Include IntInfAsInt module in libpolyml7

 -- James Clarke <jrtc27@debian.org>  Mon, 20 Mar 2017 17:08:56 +0000

polyml (5.7~20170222.git2737850-1) experimental; urgency=medium

  * New upstream version 5.7~20170222.git2737850
    - Fixes FTBFS when building the interpreted version.
  * x32.diff: New patch to fix build on x32.

 -- James Clarke <jrtc27@debian.org>  Wed, 22 Feb 2017 19:21:36 +0000

polyml (5.7~20170221.git4c3ba45-1) experimental; urgency=medium

  * New upstream version 5.7~20170221.git4c3ba45
  * Drop all patches; applied upstream
  * debian/rules: Recompile the compiler itself, since git snapshots have
    outdated pre-built import files.

 -- James Clarke <jrtc27@debian.org>  Tue, 21 Feb 2017 17:07:53 +0000

polyml (5.6-8) unstable; urgency=low

  * Bump debhelper/compat to 10
  * Run wrap-and-sort -ast
  * debian/control: Use my @debian.org email address
  * debian/copyright:
    - Use my @debian.org email address
    - Update my copyright to include 2017
  * polyc-dont-capture-build-path.diff: New patch to not capture
    -fdebug-prefix-map
  * spelling-errors.diff: New patch

 -- James Clarke <jrtc27@debian.org>  Fri, 06 Jan 2017 13:58:55 +0000

polyml (5.6-7) unstable; urgency=low

  * Make poly depend on g++, not gcc, as CXX is used by polyc, not CC. Fixes
    the upstream-polyc autopkgtest failure.

 -- James Clarke <jrtc27@jrtc27.com>  Tue, 26 Apr 2016 11:21:46 +0100

polyml (5.6-6) unstable; urgency=medium

  * Added autopkgtest support for upstream's test suite
  * New patches:
    - polyc-it.diff: Don't set "it" in polyc. This fixes the test suite when
    compiled with polyc.
    - reset-save-vector-sleep.diff: Ensure the save vector is reset when
    sleeping in Posix.Process.sleep. This stops the save vector from
    potentially overflowing and causing an assertion failure.
    - wait-return-valid-status.diff: Only return the "status" value from
    "wait" if the result is non-zero. This fixes a Fail "Unknown result
    status" exception being raised occasionally.

 -- James Clarke <jrtc27@jrtc27.com>  Mon, 25 Apr 2016 21:19:22 +0100

polyml (5.6-5) unstable; urgency=low

  * Bump up Standards-Version to 3.9.8
  * Removed patches:
    - request-completed-volatile-barrier.diff: Not a correct fix, as discussed
    with upstream in https://github.com/polyml/polyml/pull/49.
  * New patches:
    - thread-exited-interrupt-requested-overlap.diff: Fix threadExited and
    interrupt_requested overlapping. This fixes an occasional assertion
    failure on exit with the interpreted version, as well as the deadlock
    sometimes experienced on hppa.

 -- James Clarke <jrtc27@jrtc27.com>  Wed, 13 Apr 2016 12:21:00 +0100

polyml (5.6-4) unstable; urgency=low

  * New patches:
    - hppa.diff: Add support for hppa
    - maxhostnamelen.diff: Remove all use of MAXHOSTNAMELEN
    - request-completed-volatile-barrier.diff: Make MainThreadRequest
    completed field volatile (fixes test suite deadlock on hppa)
    - sh.diff: Add support for sh4
    - sparc64.diff: Add support for sparc64

 -- James Clarke <jrtc27@jrtc27.com>  Wed, 06 Apr 2016 21:21:16 +0100

polyml (5.6-3) unstable; urgency=low

  * Support for the Hurd
  * Build is now reproducible
  * Bump up Standards-Version to 3.9.7
  * New patches:
    - alpha.diff: Add support for alpha
    - bss-ioarea.diff: Export ioarea to bss section
    - m68k.diff: Add support for m68k
    - maxpathlen.diff: Remove all use of MAXPATHLEN
    - mips64.diff: Add support for mips64/mips64el
    - noexec-stack-gnu.diff: Mark stack as non-executable on all GNU systems
    - noflsh-unsigned.diff: Cast NOFLSH to unsigned (fixes a warning on the
    Hurd)
    - source-date-epoch.diff: Use SOURCE_DATE_EPOCH instead of current time if
    it is defined
    - x32.diff: Add support for x32

 -- James Clarke <jrtc27@jrtc27.com>  Sat, 12 Mar 2016 17:17:35 +0000

polyml (5.6-2) unstable; urgency=low

  * Replaced soft-float-rounding.diff with upstream's
    rounding-mode-test-allow-exceptions.diff,
    rounding-mode-test-only-allow-exceptions-for-set-mode.diff, soft-fpu.diff
    and disable-compile-time-real-eval.diff. Avoids modifying the test driver,
    instead skipping the test if unsupported directly inside the test itself.
  * New patches (excluding soft-float-rounding.diff replacements):
    - fix-heap-ratio-assert.diff: Backport upstream's fix to let the
      calculated minimum heap size be the same as the desired value.
      Previously it failed an assertion.
    - fix-script-args.diff: Backport upstream's fix so --script can be used
      with additional command-line arguments.
    - mips-abi.diff: Mark polyexport.o as CPIC on MIPS when code is
      position-independent. Fixes ld warning about linking abicalls files with
      non-abicalls files.
    - unix-const-vec-unsigned.diff: Define unixConstVec as unsigned. Fixes
      narrowing conversion inside braces on PowerPC with GCC 6.
    - use-polyunsigned-consistently-for-stream-ids.diff: Backport upstream's
      fix so POLYUNSIGNED is used everywhere to refer to stream IDs, avoiding
      implicit narrowing conversions. Includes fixed realloc failure handling.

 -- James Clarke <jrtc27@jrtc27.com>  Mon, 01 Feb 2016 00:08:29 +0000

polyml (5.6-1) unstable; urgency=low

  * New upstream version
  * Removed patches applied upstream
  * Rename libpolyml6 to libpolyml7
  * New patches:
    - ioctl-int.diff: Use int instead of unsigned long for ioctl calls. Fixes
      Tests/Succeed/Test083.ML on 64-bit big-endian architectures.
    - s390.diff: Add support for S/390 (both s390 and s390x).
    - soft-float-rounding.diff: Don't support rounding modes for soft-float
      systems. Skips Tests/Succeed/Test121.ML on armel.
    - streamtoken-endian.diff: Use a POLYUNSIGNED for StreamToken.streamNo.
      Fixes stream issues on 64-bit big-endian architectures.

 -- James Clarke <jrtc27@jrtc27.com>  Mon, 25 Jan 2016 23:09:59 +0000

polyml (5.5.2-4) unstable; urgency=low

  * Take over as uploader
  * New patches:
    - make-check.diff: Backport upstream's fix so 'make check' runs the
      regression test suite
    - mips.diff: Backport upstream's support for mips
    - mipsel.diff: Backport upstream's support for mipsel
    - pexport-endian.diff: Backport upstream's fix for big-endian
      architectures
    - ppc64.diff: Backport upstream's support for ppc64 and ppc64el

 -- James Clarke <jrtc27@jrtc27.com>  Sun, 24 Jan 2016 13:45:16 +0000

polyml (5.5.2-3) unstable; urgency=low

  * Team upload.
  * Backport upstream's arm64 support (Closes: #802341)
  * Added file dependency (required by polyc)

 -- James Clarke <jrtc27@jrtc27.com>  Tue, 20 Oct 2015 22:37:47 +0100

polyml (5.5.2-2) unstable; urgency=low

  * Team upload.
  * Enable all hardening flags (currently adds -fPIE, -pie and -Wl,-z,now)
  * Added gcc dependency (required by polyc)
  * Added libffi-dev dependency so polyc can link compiled code against it

 -- James Clarke <jrtc27@jrtc27.com>  Sat, 17 Oct 2015 15:14:39 +0100

polyml (5.5.2-1) unstable; urgency=low

  * Team upload.
  * New upstream version (Closes: #561763)
  * Removed libtool .la files
  * Rename libpolyml1 to libpolyml6
  * Bump up Standards-Version to 3.9.6
  * Bump up debian/compat to 9
  * Made descriptions more consistent
  * Convert to dh(1) format
  * Use autoreconf over autotools-dev
  * Add Multi-Arch fields
  * Update copyright
  * Build on Architecture: any
  * Patch libpolyml/x86asm.asm to stop the stack being executable
  * Remove postinst script as debhelper will generate triggers for ldconfig
  * Use Debian's libffi

 -- James Clarke <jrtc27@jrtc27.com>  Thu, 15 Oct 2015 22:20:13 +0100

polyml (5.2.1-1.1) unstable; urgency=low

  * NMU, added armhf to the arch list. (Closes: #623135)

 -- Konstantinos Margaritis <markos@debian.org>  Wed, 11 Jan 2012 22:39:20 +0000

polyml (5.2.1-1) unstable; urgency=low

  [ Achim D. Brucker ]
  * Initial upload to Debian (Closes: #494488)
  * new upstream version

  [ Lionel Elie Mamane ]
  * Various packaging fixes
  * Bump up Standards-Version to 3.8.2

 -- Lionel Elie Mamane <lmamane@debian.org>  Thu, 03 Sep 2009 15:08:33 +0200

polyml (5.2-3) unstable; urgency=low

  * Fixed copyright

 -- Achim D. Brucker <brucker@member.fsf.org>  Mon, 16 Jun 2008 15:03:57 +0200

polyml (5.2-2) unstable; urgency=low

  * Fixed lintian warnings

 -- Achim D. Brucker <brucker@member.fsf.org>  Sun, 15 Jun 2008 21:05:42 +0200

polyml (5.2-1) unstable; urgency=low

  * Initial release

 -- Achim D. Brucker <brucker@member.fsf.org>  Sun, 15 Jun 2008 11:06:07 +0200

